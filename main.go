package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"runtime"
	"time"

	"github.com/gobuffalo/packr"
)

var osUser string

func open(url string) error {

	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}

	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func server(filePath string, output string, osUser string) {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, output)
	})

	http.HandleFunc("/ref", func(w http.ResponseWriter, r *http.Request) {
		err := watch(filePath)

		if err != nil {
			fmt.Println("Error:", err)
		} else {
			createMap(filePath, output, osUser)
			http.ServeFile(w, r, output)
		}
	})

	log.Fatal(http.ListenAndServe(":8081", nil))
}

func createMap(filePath string, output string, osUser string) {

	var markmap string
	switch runtime.GOOS {
	case "windows":
		markmap = "markmap-lib.exe"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		markmap = "/home/" + osUser + "/.local/bin/markmap-lib"
	}

	cmdMap := &exec.Cmd{
		Path:   markmap,
		Args:   []string{"markmap", "--no-open", "-o", output, filePath},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}

	if err := cmdMap.Run(); err != nil {
		fmt.Println("Error:", err)
	}
}

func watch(filePath string) error {

	initialStat, err := os.Stat(filePath)

	if err != nil {
		return err
	}

	for {
		stat, err := os.Stat(filePath)

		if err != nil {
			return err
		}

		if stat.Size() != initialStat.Size() || stat.ModTime() != initialStat.ModTime() {
			break
		}

		time.Sleep(1 * time.Second)
	}

	return nil

}

func main() {
	var filePath string
	var output string
	flag.StringVar(&filePath, "f", "./", "Path of markdown file [string]")
	flag.StringVar(&output, "o", "./", "Name of output file [string]")
	flag.Parse()
	var osExec string
	var osLib string
	var osBox string

	switch runtime.GOOS {
	case "windows":
		osExec = "markmap-lib-win.exe"
		osLib = "markmap-lib.exe"
		osBox = "./windows_lib"
		user, err := user.Current()
		if err != nil {
			fmt.Println("Error:", err)
			os.Exit(2)
		}
		osUser = user.Username

	default: // "linux", "freebsd", "openbsd", "netbsd"
		osExec = "markmap-lib"
		user, err := user.Current()

		if err != nil {
			fmt.Println("Error:", err)
			os.Exit(2)
		}

		osUser = user.Username
		osLib = "/home/" + osUser + "/.local/bin/markmap-lib"
		osBox = "./linux_lib"
	}

	box := packr.NewBox(osBox)
	if _, err := os.Stat(osLib); os.IsNotExist(err) {
		fmt.Println("installing markmap-lib")
		f, err := os.Create(osLib)
		if err != nil {
			fmt.Println("Error:", err)
		}
		f.WriteString(box.String(osExec))
		f.Close()
		err = os.Chmod(osLib, 0755)
		if err != nil {
			log.Println(err)
		}
	}

	createMap(filePath, output, osUser)
	go server(filePath, output, osUser)
	open("http://localhost:8081/")
	for {
	}
}
