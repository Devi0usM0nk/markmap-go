# Desceiption
This a small wrapper script around [markmap-lib](https://github.com/gera2ld/markmap-lib) by [gera2ld](https://github.com/gera2ld). The script will convert your markdown file to an html mindmap using markmap-lib and start a server hosting and open the rendered html in your default browser. Whenever a change to document is made and saved then it will also update the mindmap.
## Usage
* Linux
```sh
$ markmap-go -f <Path to markdown file> -o <output filename with html as extension>
```

* Windows
```cmd
C:/ markmap-go -f <Path to markdown file> -o <output filename with html as extension>
```
This start the  server and open the map in your browser. Now, You can start editing your markdown file with your favourite editor.
## Installation
Golang is required for installation. Once you have golang installed run following commands in cmd or linux terminal.

```sh
$ cd markmap-go-master
$ go get -u github.com/gobuffalo/packr
$ packr && go build && packr clean
```
Now there would be a binary generated in linux system and an `.exe` file in windows system that you can use.
